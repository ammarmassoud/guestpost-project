const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const morgan = require("morgan");
const path = require("path");

const app = express();

app.use(express.static(path.resolve(__dirname, "node_modules/bootstrap/dist")));
app.use(express.static(path.resolve(__dirname, "node_modules/@popperjs/core/lib")));

app.set("views", path.resolve(__dirname, "views"));
app.set("view engine", "ejs");

var entries = [];
app.locals.entries = entries;

app.use(morgan("dev"));

app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/new-entry", (req, res) => {
  res.render("new-entry");
});

app.post("/new-entry", (req, res) => {
  if (!req.body.title || !req.body.desc) {
    res.status(400).send("Entries Must Have a Title and a Description");
    return;
  }
  entries.push({
    title: req.body.title,
    desc: req.body.desc,
    published: new Date().toLocaleTimeString(),
  });
  res.redirect("/");
});

app.use(function (req, res) {
  res.status(400).render("error");
});

http.createServer(app).listen(3030, () => {
  console.log("GuestPost App Starts on Port 3030");
});
